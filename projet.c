#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

#define MAX_LENGTH 100

/*Lecture d'une commande;
  Tant que la commande de sortie n'est pas tapée
	Analyse de la commande;
	Si la commande correspond à une fonction interne
	alors application de cette fonction aux arguments de la
		commande;
	sinon création d'un processus fils qui exécutera la commande;
		et le processus père attend la mort du processus fils;
	Finsi
	Lecture d'une commande;
  Fintantque
 * 
 * */


void lireLigne(char*);
void lireCom(char* cmd, char** com);


void mon_shell(char *arge[]){
	char cmd[MAX_LENGTH];
	char *com[10];
	printf("Voici mon shell, taper Q pour sortir\n");
	printf("> ");
	lireLigne(cmd);
	
	while(strcmp(cmd,"exit")){
		lireCom(cmd,com);
		if(strcmp(com[0],"echo")==0){
			printf("%s\n", com[1]);
		}
		
		lireLigne(cmd);
	}

}

void lireCom(char* cmd, char** com){
		int i =1;
		com[0] = strtok (cmd," ");
		while (i < 10 && com[i-1] != NULL ) {
			com[i] = strtok( NULL, " " );
			i++;
		}
}

void lireLigne(char* ligne){	
	char* s;
	fgets(ligne, MAX_LENGTH, stdin);
	
	if(strcmp(ligne,"\n")==0){
		ligne ="";	
	}
	s = strchr(ligne,'\n');

	if ( s != 0){
		*s='\0';
	}
}

int main(int argc,char *argv[],char *arge[]){
	mon_shell(arge);
	return 0;
}

