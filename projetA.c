#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

#define MAX_LENGTH 50

/*Lecture d'une commande;
  Tant que la commande de sortie n'est pas tapée
	Analyse de la commande;
	Si la commande correspond à une fonction interne
	alors application de cette fonction aux arguments de la
		commande;
	sinon création d'un processus fils qui exécutera la commande;
		et le processus père attend la mort du processus fils;
	Finsi
	Lecture d'une commande;
  Fintantque
 * 
 * */
 void lireLigne(char*);
 
 void mon_shell(char *arge[]){
	 char cmd[MAX_LENGTH];
	 printf("Voici mon shell, taper Q pour sortir\n");
	 printf("> ");
	 
	 lireLigne(cmd);

}

 void lireLigne(char* ligne){
	 char fr[MAX_LENGTH];
	 char *s;
	 fgets(fr, MAX_LENGTH, stdin);

	if ( s = strchr(fr,'\n')){
		*s='\0';
		}
	printf("a %s a", fr);
	 }
 
 int main(int argc,char *argv[],char *arge[]){
	mon_shell(arge);
	return 0;
}
